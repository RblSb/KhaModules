package game;

typedef TileProps = {
	collide:Bool,
	type:Slope
}

typedef TsTileProps = {
	?collide:Bool,
	?type:Slope
}
