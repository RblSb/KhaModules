package game;

import khm.tilemap.Tilemap;
import khm.Types.Point;
import khm.Types.Size;

private typedef Coll = {
	state:Bool,
	left:Bool,
	right:Bool,
	up:Bool,
	down:Bool
}

class TileCollision {

	var tilemap:Tilemap;
	public var min:Float;
	public var layer:Int;
	var tsize(get, never):Int;
	function get_tsize():Int return tilemap.tileSize;
	final coll:Coll = {
		state: false,
		left: false,
		right: false,
		up: false,
		down: false
	}

	public function new(tilemap:Tilemap, layer:Int, minSize:Float) {
		this.tilemap = tilemap;
		this.layer = layer;
		min = minSize;
	}

	public function update(pos:Point, size:Size, speed:Point):Coll {
		coll.state = false;
		coll.left = false;
		coll.right = false;
		coll.up = false;
		coll.down = false;

		var sx = Math.abs(speed.x);
		var sy = Math.abs(speed.y);
		final vx = sx / speed.x;
		final vy = sy / speed.y;

		while (sx > min || sy > min) {
			if (sx > min) {
				pos.x += min * vx;
				sx -= min;
				collision(coll, pos, size, speed, 0);
				if (coll.state) sx = 0;
			}

			if (sy > min) {
				pos.y += min * vy;
				sy -= min;
				collision(coll, pos, size, speed, 1);
				if (coll.state) sy = 0;
			}
		}

		if (sx > 0) {
			pos.x += sx * vx;
			collision(coll, pos, size, speed, 0);
		}

		if (sy > 0) {
			pos.y += sy * vy;
			collision(coll, pos, size, speed, 1);
		}

		pos.x -= speed.x;
		pos.y -= speed.y;
		return coll;
	}

	function collision(coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		final x = Std.int(pos.x / tsize);
		final y = Std.int(pos.y / tsize);
		final maxX = Math.ceil((pos.x + size.w) / tsize);
		final maxY = Math.ceil((pos.y + size.h) / tsize);

		for (iy in y...maxY) {
			for (ix in x...maxX) {
				final props:TileProps = tilemap.getTile(layer, ix, iy).props;
				if (!props.collide) continue;
				block(ix, iy, coll, pos, size, speed, dir, props.type);
			}
		}
	}

	inline function block(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int, slope:Slope):Void {
		switch (slope) {
			case FULL: FULL(ix, iy, coll, pos, size, speed, dir);
			case HALF_B: HALF_B(ix, iy, coll, pos, size, speed, dir);
			case HALF_T: HALF_T(ix, iy, coll, pos, size, speed, dir);
			case HALF_L: HALF_L(ix, iy, coll, pos, size, speed, dir);
			case HALF_R: HALF_R(ix, iy, coll, pos, size, speed, dir);
			case HALF_BL: HALF_BL(ix, iy, coll, pos, size, speed, dir);
			case HALF_BR: HALF_BR(ix, iy, coll, pos, size, speed, dir);
			case HALF_TL: HALF_TL(ix, iy, coll, pos, size, speed, dir);
			case HALF_TR: HALF_TR(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_BL: QUARTER_BL(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_BR: QUARTER_BR(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_TL: QUARTER_TL(ix, iy, coll, pos, size, speed, dir);
			case QUARTER_TR: QUARTER_TR(ix, iy, coll, pos, size, speed, dir);
			case NONE:
			default: throw 'Unknown slope type $slope';
		}
	}

	inline function tileLeft(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point):Void {
		coll.state = true;
		coll.left = true;
		pos.x = ix * tsize + tsize;
		speed.x = 0;
	}

	inline function tileRight(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point):Void {
		coll.state = true;
		coll.right = true;
		pos.x = ix * tsize - size.w;
		speed.x = 0;
	}

	inline function tileBottom(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point):Void {
		coll.state = true;
		coll.down = true;
		pos.y = iy * tsize - size.h;
		speed.y = 0;
	}

	inline function tileTop(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point):Void {
		coll.state = true;
		coll.up = true;
		pos.y = iy * tsize + tsize;
		speed.y = 0;
	}

	inline function FULL(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (dir == 0) {
			if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
			else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
		} else if (dir == 1) {
			if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
			else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
		}
	}

	inline function HALF_B(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.y + size.h > iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) {
					coll.state = true;
					coll.down = true;
					pos.y = iy * tsize + tsize / 2 - size.h;
					speed.y = 0;
				} else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function HALF_T(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.y < iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) {
					coll.state = true;
					coll.up = true;
					pos.y = iy * tsize + tsize / 2;
					speed.y = 0;
				}
			}
		}
	}

	inline function HALF_L(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.x < ix * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) {
					coll.state = true;
					coll.left = true;
					pos.x = ix * tsize + tsize / 2;
					speed.x = 0;
				} else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function HALF_R(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.x + size.w > ix * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) {
					coll.state = true;
					coll.right = true;
					pos.x = ix * tsize + tsize / 2 - size.w;
					speed.x = 0;
				}
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}
	//TODO implement triangle collision
	inline function HALF_BL(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {}

	inline function HALF_BR(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {}

	inline function HALF_TL(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {}

	inline function HALF_TR(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {}

	inline function QUARTER_BL(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.x < ix * tsize + tsize / 2 &&
			pos.y + size.h > iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) {
					coll.state = true;
					coll.left = true;
					pos.x = ix * tsize + tsize / 2;
					speed.x = 0;
				} else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) {
					coll.state = true;
					coll.down = true;
					pos.y = iy * tsize + tsize / 2 - size.h;
					speed.y = 0;
				} else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function QUARTER_BR(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.x + size.w > ix * tsize + tsize / 2 &&
			pos.y + size.h > iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) {
					coll.state = true;
					coll.right = true;
					pos.x = ix * tsize + tsize / 2 - size.w;
					speed.x = 0;
				}
			} else if (dir == 1) {
				if (speed.y > 0) {
					coll.state = true;
					coll.down = true;
					pos.y = iy * tsize + tsize / 2 - size.h;
					speed.y = 0;
				} else if (speed.y < 0) tileTop(ix, iy, coll, pos, size, speed);
			}
		}
	}

	inline function QUARTER_TL(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.x < ix * tsize + tsize / 2 &&
			pos.y < iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) {
					coll.state = true;
					coll.left = true;
					pos.x = ix * tsize + tsize / 2;
					speed.x = 0;
				} else if (speed.x > 0) tileRight(ix, iy, coll, pos, size, speed);
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) {
					coll.state = true;
					coll.up = true;
					pos.y = iy * tsize + tsize / 2;
					speed.y = 0;
				}
			}
		}
	}

	inline function QUARTER_TR(ix:Int, iy:Int, coll:Coll, pos:Point, size:Size, speed:Point, dir:Int):Void {
		if (pos.x + size.w > ix * tsize + tsize / 2 &&
			pos.y < iy * tsize + tsize / 2) {
			if (dir == 0) {
				if (speed.x < 0) tileLeft(ix, iy, coll, pos, size, speed);
				else if (speed.x > 0) {
					coll.state = true;
					coll.right = true;
					pos.x = ix * tsize + tsize / 2 - size.w;
					speed.x = 0;
				}
			} else if (dir == 1) {
				if (speed.y > 0) tileBottom(ix, iy, coll, pos, size, speed);
				else if (speed.y < 0) {
					coll.state = true;
					coll.up = true;
					pos.y = iy * tsize + tsize / 2;
					speed.y = 0;
				}
			}
		}
	}

}
