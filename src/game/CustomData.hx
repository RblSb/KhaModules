package game;

import khm.tilemap.TsCustomizer;
import khm.tilemap.Tileset.TsProps;
import khm.tilemap.Tilemap.GameObject;

class CustomData extends TsCustomizer {

	override function initProps(data:TsProps):Any {
		final props:TileProps.TsTileProps = data.props;
		if (props.collide == null) props.collide = false;
		if (props.type == null) props.type = props.collide ? "FULL" : "NONE";
		else props.type = Slope.fromString(cast props.type);

		if (props.type == null) throw "haxe loose nullsafety fix";
		switch (data.transformation) {
			case Rotate90: props.type = Slope.rotate(props.type);
			case Rotate180: for (i in 0...2) props.type = Slope.rotate(props.type);
			case Rotate270: for (i in 0...3) props.type = Slope.rotate(props.type);
			case FlipX: props.type = Slope.flipX(props.type);
			case FlipY: props.type = Slope.flipY(props.type);
			case null:
		}

		return props;
	}

	override function objectTemplate(layer:Int, tile:Int):Null<GameObject> {
		inline function obj(type:String, ?data:Any):GameObject {
			return {type: type, layer: layer, x: -1, y: -1, data: data};
		}
		return switch (layer) {
			case 0: null;
			case 1:
				switch (tile) {
					default: null;
				}
			case 2:
				switch (tile) {
					case 1: obj("player");
					case 2: obj("end");
					case 3: obj("death");
					case 4: obj("save");
					case 5: obj("text", {text: "", author: ""});
					default: null;
				}
			default: null;
		}
	}

}
