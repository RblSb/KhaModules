package game;

import kha.Canvas;
import kha.Assets;
import kha.graphics2.Graphics;
import kha.input.KeyCode;
// import khm.editor.Editor;
import khm.Screen;
import khm.Screen.Pointer;
import khm.tilemap.Tilemap;
import khm.tilemap.Tilemap.GameMapJSON;
import khm.tilemap.Tileset;

typedef Player = {
	x:Float, y:Float,
	w:Float, h:Float,
	sx:Float, sy:Float,
	onGround:Bool
}

class Game extends Screen {

	var collision:Null<TileCollision>;
	var player:Player = {
		x: 0, y: 0, w: 0, h: 0,
		sx: 0, sy: 0, onGround: false
	};
	final tilemap:Tilemap;
	var tileSize(get, never):Int;
	function get_tileSize():Int return tilemap.tileSize;

	public function new():Void {
		final blob = Assets.blobs.tileset_json;
		final tileset = new Tileset(blob, new CustomData());
		tilemap = new Tilemap(tileset);
		super();
	}

	public function init():Void {
		final map:GameMapJSON = {
			w: 13,
			h: 8,
			layers: [
				[
					[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
					[0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
					[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0],
					[0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0],
					[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
				]
			],
			objects: [],
			floatObjects: []
		};
		tilemap.loadJSON(map);
		collision = new TileCollision(tilemap, 0, tileSize / 4);
		restart();

		// final editor = new Editor();
		// editor.init(tileset);
		// editor.onMapJSONLoad(map);
		// editor.show();
	}

	function restart():Void {
		player.x = tileSize * 5;
		player.y = 0;
		player.w = tileSize / 2;
		player.h = tileSize;
		player.sx = 0;
		player.sy = 0;
	}

	public static inline function isMapExists(id:Int):Bool {
		return Assets.blobs.get('maps_${id}_json') != null;
	}

	public function loadMapId(id:Int):Void {
		final data = Assets.blobs.get('maps_${id}_json');
		tilemap.loadJSON(haxe.Json.parse(data.toString()));
	}

	override function onKeyDown(key:KeyCode):Void {
		if (key == 189 || key == HyphenMinus) {
			if (scale > 1) setScale(scale - 1);

		} else if (key == 187 || key == Equals || key == Plus) {
			if (scale < 9) setScale(scale + 1);
		}
	}

	override function onMouseDown(p:Pointer):Void {}

	override function onResize():Void {
		tilemap.camera.w = Screen.w;
		tilemap.camera.h = Screen.h;
	}

	override function onRescale(scale:Float):Void {
		tilemap.scale = scale;
	}

	override function onUpdate():Void {
		playerControl();
		playerUpdate();
		tilemap.camera.strictCenter(cast player);
	}

	function playerControl():Void {
		var speed = tileSize / 60;
		if ((keys[Left] || keys[A]) && player.sx > -3) player.sx -= speed;
		if ((keys[Right] || keys[D]) && player.sx < 3) player.sx += speed;
		if (keys[Down] || keys[S]) player.sy = 999;
		if (keys[E]) player.sx = tileSize * 2;
		if (keys[Q]) player.sx = -tileSize * 2;

		if ((keys[Up] || keys[W]) && player.onGround) {
			player.onGround = false;
			player.sy = -tileSize / 5;
		}

		if (keys[R]) restart();
	}

	function playerUpdate():Void {
		player.sy += 0.1;
		final pos = {x: player.x, y: player.y};
		final size = {w: player.w, h: player.h};
		final speed = {x: player.sx, y: player.sy};
		if (collision == null) throw "collision is null";
		final coll = collision.update(pos, size, speed);

		player.x = pos.x + speed.x;
		player.y = pos.y + speed.y;
		player.sx = speed.x;
		player.sy = speed.y;
		player.onGround = coll.down;

		if (player.sx >= 0.1) player.sx -= 0.1;
		if (player.sx <= -0.1) player.sx += 0.1;
		if (Math.abs(player.sx) < 0.1) player.sx = 0;
	}

	override function onRender(canvas:Canvas):Void {
		final g = canvas.g2;
		g.begin(0xFFA0A0A0);
		g.font = Assets.fonts.OpenSans_Regular;
		g.color = 0xFFFFFFFF;

		drawTileset(g);
		tilemap.drawLayers(g);
		drawPlayer(g);

		g.color = 0x77FF0000;
		for (p in pointers)
			if (p.isActive) g.fillRect(p.x, p.y, 5, 5);
		g.end();
	}

	function drawPlayer(g:Graphics):Void {
		g.color = 0xFFF9A520;
		final scale = tilemap.scale;
		final camX = tilemap.camera.x;
		final camY = tilemap.camera.y;
		g.fillRect(
			Std.int(player.x * scale) / scale + camX,
			Std.int(player.y * scale) / scale + camY,
			player.w, player.h
		);
	}

	function drawTileset(g:Graphics):Void {
		#if debug
		final tileset = @:privateAccess tilemap.tileset;
		g.drawScaledImage(
			tileset.img, 0, 0,
			tileset.img.width / scale,
			tileset.img.height / scale
		);
		#end
	}

}
