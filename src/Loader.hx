package;

import kha.Framebuffer;
import kha.graphics2.Graphics;
import kha.System;
import kha.Assets;
import khm.Settings;
import khm.Screen;
import khm.Lang;

typedef Save = {
	version:Int,
	?touchMode:Bool,
	?lang:String
}

class Loader {

	public function new() {}

	public function init():Void {
		System.notifyOnFrames(onRender);
		Assets.loadEverything(loadComplete);
	}

	public function loadComplete():Void {
		System.removeFramesListener(onRender);

		Settings.init({
			version: 1
		});
		final sets:Save = Settings.read();
		Screen.init({isTouch: sets.touchMode});
		Lang.loadFolder("langs");
		Lang.set(sets.lang);
		Graphics.fontGlyphs = Lang.fontGlyphs;

		#if kha_html5
		final nav = js.Browser.window.location.hash.substr(1);
		switch (nav) {
			case "editor":
				final file = Assets.blobs.tileset_json;
				final tileset = new khm.tilemap.Tileset(file);
				final editor = new khm.editor.Editor(tileset);
				editor.show();
				editor.init();
			case "game":
				newGame();
			case "imgui":
				final demo = new demo.Gui();
				demo.show();
				demo.init();
			default:
				newGame();
		}
		#else
		newGame();
		#end
	}

	function newGame():Void {
		final game = new game.Game();
		game.show();
		game.init();
	}

	function onRender(fbs:Array<Framebuffer>):Void {
		final g = fbs[0].g2;
		g.begin(true, 0xFFFFFFFF);
		final h = System.windowHeight() / 20;
		final w = Assets.progress * System.windowWidth();
		final y = System.windowHeight() / 2 - h;
		g.color = 0xFF000000;
		g.fillRect(0, y, w, h * 2);
		g.end();
	}

}
