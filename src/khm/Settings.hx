package khm;

import kha.Storage;
import kha.StorageFile;

private typedef Vers = {version: Int};
private typedef Updater = (data:Any, version:Int)->Any;

class Settings {

	static var defaults:Null<Vers>;
	static var updater:Null<Updater>;

	public static function init(def:Vers, ?upd:Updater):Void {
		defaults = def;
		updater = upd;
	}

	public static function read():Any {
		#if hl return defaults; #end
		final file = Storage.defaultFile();
		final data:Any = file.readObject();
		return checkData(data);
	}

	static function checkData(data:Vers):Any {
		if (defaults == null) throw "read: default data is null";
		if (data == null) return defaults;
		if (data.version == defaults.version) return data;
		if (data.version > defaults.version)
			throw "read: current data version is larger than default data version";
		if (updater == null) throw "read: updater function is null";
		while (data.version < defaults.version) {
			data = updater(data, data.version);
			data.version++;
		}
		write(data);
		return data;
	}

	public static function set(sets:Any):Void {
		final data = read();
		final fields = Reflect.fields(sets);
		for (field in fields) {
			final value = Reflect.field(sets, field);
			Reflect.setField(data, field, value);
		}
		write(data);
	}

	public static function write(data:Vers):Void {
		#if hl return; #end
		final file:StorageFile = Storage.defaultFile();
		file.writeObject(data);
	}

	public static function reset():Void {
		if (defaults == null) throw "reset: default data is null";
		write(defaults);
	}

}
