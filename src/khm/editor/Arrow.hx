package khm.editor;

import kha.graphics2.Graphics;
import khm.editor.Interfaces.Tool;
import khm.editor.ui.Modal;
import khm.Screen.Pointer;
import khm.tilemap.Tilemap;
import khm.tilemap.Tilemap.GameObject;
import khm.Types.IPoint;
import khm.Types.IRect;
import haxe.Json;

class Arrow implements Tool {

	var editor:Editor;
	var tilemap:Tilemap;
	var tileSize(get, never):Int;
	function get_tileSize():Int return tilemap.tileSize;
	final start:IPoint = {x: -1, y: -1};
	final end:IPoint = {x: -1, y: -1};

	public function new(editor:Editor, tilemap:Tilemap) {
		this.editor = editor;
		this.tilemap = tilemap;
	}

	public function clearHistory():Void {}

	public function undo():Void {}

	public function redo():Void {}

	public function onMouseDown(p:Pointer, layer:Int, x:Int, y:Int, tile:Int):Void {
		start.x = x;
		start.y = y;
		end.x = x;
		end.y = y;
	}

	public function onMouseMove(p:Pointer, layer:Int, x:Int, y:Int, tile:Int):Void {
		if (!p.isDown) return;
		if (end.x == x && end.y == y) return;
		end.x = x;
		end.y = y;
		final rect:IRect = makeRect(start, end);
		editor.setSelection(rect);
	}

	public function onMouseUp(p:Pointer, layer:Int, x:Int, y:Int, tile:Int):Void {
		if (start.x == x && start.y == y) {
			editor.setSelection(null);
			action(layer, x, y, tile);
			return;
		}
		end.x = x;
		end.y = y;
		final rect:IRect = makeRect(start, end);
		editor.setSelection(rect);
	}

	function makeRect(p:IPoint, p2:IPoint):IRect {
		final sx = p.x < p2.x ? p.x : p2.x;
		final sy = p.y < p2.y ? p.y : p2.y;
		final ex = p.x < p2.x ? p2.x : p.x;
		final ey = p.y < p2.y ? p2.y : p.y;
		return {x: sx, y: sy, w: ex - sx, h: ey - sy};
	}

	public function onUpdate():Void {}

	public function onRender(g:Graphics):Void {}

	function action(layer:Int, x:Int, y:Int, tile:Int):Void {
		var objs = tilemap.getObjects(layer, x, y);
		if (objs.length == 0) { // check other layers
			for (i in 0...tilemap.map.layers.length) {
				layer = i;
				objs = tilemap.getObjects(layer, x, y);
				if (objs.length != 0) break;
			}
		}
		if (objs.length == 0) return;
		#if kha_html5
		Modal.prompt("Object:", Json.stringify(objs, "  "), function(data:Null<String>) {
			if (data == null) return;
			final objs:Array<GameObject> = Json.parse(data); // TODO catch parsing errors
			if (objs != null) tilemap.setObjects(layer, x, y, objs);
		});
		#end
	}

}
