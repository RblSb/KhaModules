package khm.editor;

import kha.graphics2.Graphics;
import khm.tilemap.Tilemap;
import khm.Screen;
import khm.Screen.Pointer;

class TilePanel {

	static inline var BG_COLOR = 0xAA000000;
	static inline var GRID_COLOR = 0x50000000;
	static inline var SELECT_COLOR = 0xAAFFFFFF;
	static inline var OVER_ALPHA = 1;
	static inline var OUT_ALPHA = 0.5;
	static inline var MIN_COLUMNS = 2;
	static inline var MAX_COLUMNS = 10;
	public var x = 0;
	public var y = 0;
	public var w = 0;
	public var h = 0;
	var editor:Editor;
	var tilemap:Tilemap;
	var tileSize(get, never):Int;
	function get_tileSize():Int return tilemap.tileSize;
	var opacity = OUT_ALPHA;
	var minW = MIN_COLUMNS;
	var maxW = MAX_COLUMNS;
	var customW = 0;
	var current = 0;
	// var firstEmptyLine = true; // TODO

	public function new(editor:Editor, tilemap:Tilemap) {
		this.editor = editor;
		this.tilemap = tilemap;
		resize();
	}

	public function incColumns():Void {
		if (customW < w) customW = w;
		customW++;
	}

	public function decColumns():Void {
		customW--;
		if (customW < 0) customW = 0;
	}

	public function onDown(p:Pointer):Bool {
		var result = false;
		if (isInside(p.x, p.y)) {
			setTile(p);
			result = true;
		}
		return result;
	}

	function setTile(p:Pointer):Void {
		final tx = Std.int((p.x - x) / tileSize);
		final ty = Std.int((p.y - y) / tileSize);
		final cord = ty * w + tx;
		final pos = countTilePos(cord);
		if (pos.layer == editor.tilesLengths.length) return;
		if (pos.tile != 0) editor.layer = pos.layer;
		editor.tile = pos.tile;
	}

	function countTilePos(tile:Int):{tile:Int, layer:Int} {
		var layer = 0;
		for (len in editor.tilesLengths) {
			if (tile > len) {
				tile -= len;
				layer++;
			} else break;
		}
		return {tile: tile, layer: layer};
	}

	public function onMove(p:Pointer):Bool {
		var result = false;
		if (isInside(p.x, p.y)) {
			opacity = OVER_ALPHA;
			result = true;
		} else opacity = OUT_ALPHA;
		return result;
	}

	public function onUp(p:Pointer):Bool {
		var result = false;
		if (isInside(p.x, p.y)) {
			result = true;
		}
		return result;
	}

	inline function isInside(x:Int, y:Int):Bool {
		if (x < this.x || x >= this.x + w * tileSize || y < this.y || y >= this.y + h * tileSize) return false;
		return true;
	}

	public function resize():Void {
		update();
	}

	public function update():Void {
		current = currentTile();
		var tiles = 1;
		for (i in editor.tilesLengths) tiles += i;

		w = minW;
		if (customW > 0) w = customW;
		for (i in 0...maxW - w) {
			h = Math.ceil(tiles / w);
			if (y + h * tileSize > Screen.h) w++;
		}
		h = Math.ceil(tiles / w);
		x = Screen.w - tileSize * w;
	}

	inline function currentTile():Int {
		final tilesLengths = editor.tilesLengths;
		var id = editor.tile;
		for (i in 0...editor.layer) id += tilesLengths[i];
		return id;
	}

	public function render(g:Graphics):Void {
		g.opacity = opacity;
		drawBg(g, x, y, w, h);
		drawGrid(g, x, y, w, h);
		drawTiles(g, x, y, w, h);
		drawSelection(g, x, y, w, h);
		g.opacity = 1;
	}

	function drawBg(g:Graphics, x:Int, y:Int, w:Int, h:Int):Void {
		g.color = BG_COLOR;
		g.fillRect(x - 0.5, y - 0.5, w * tileSize + 1, h * tileSize + 1);
	}

	function drawTiles(g:Graphics, x:Int, y:Int, w:Int, h:Int):Void {
		final tilesLengths = editor.tilesLengths;
		var offX = tileSize;
		// if (firstEmptyLine) offX = w * tileSize;
		var tx = 0;
		var ty = 0;
		g.color = 0xFFFFFFFF;
		for (l in 0...tilesLengths.length) {
			for (id in 1...tilesLengths[l] + 1) {
				tx = offX % (w * tileSize);
				ty = Std.int(offX / (w * tileSize)) * tileSize;
				editor.drawTile(g, l, x + tx, y + ty, id);
				offX += tileSize;
			}
			// fill layer line
			// if (firstEmptyLine) offX += w * tileSize - offX % (w * tileSize);
		}
	}

	function drawGrid(g:Graphics, x:Int, y:Int, w:Int, h:Int):Void {
		var offX = 0;
		var ix = 0;
		var iy = 0;
		g.color = GRID_COLOR;
		for (i in 0...w * h) {
			g.drawRect(x + ix, y + iy, tileSize, tileSize);
			offX += tileSize;
			ix = offX % (w * tileSize);
			iy = Std.int(offX / (w * tileSize)) * tileSize;
		}
		g.drawLine(x, y, x, y + iy);
	}

	function drawSelection(g:Graphics, x:Int, y:Int, w:Int, h:Int):Void {
		final offX = editor.tile == 0 ? 0 : current * tileSize;
		final ix = offX % (w * tileSize);
		final iy = Std.int(offX / (w * tileSize)) * tileSize;
		g.color = SELECT_COLOR;
		g.drawRect(x + ix, y + iy, tileSize, tileSize);
	}

}
