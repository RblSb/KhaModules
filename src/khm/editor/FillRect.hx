package khm.editor;

import kha.graphics2.Graphics;
import khm.editor.Interfaces.Tool;
import khm.editor.Types.ArrHistory;
import khm.tilemap.Tilemap;
import khm.Screen.Pointer;
import khm.Types.IPoint;
import khm.Types.IRect;

class FillRect implements Tool {

	final undoH:Array<ArrHistory> = [];
	final redoH:Array<ArrHistory> = [];
	var maxHistory = 10;
	var editor:Editor;
	var tilemap:Tilemap;
	var start:Null<IPoint>;
	var end:Null<IPoint>;

	public function new(editor:Editor, tilemap:Tilemap) {
		this.editor = editor;
		this.tilemap = tilemap;
	}

	function addHistory(h:ArrHistory):Void {
		undoH.push(h);
		if (undoH.length > maxHistory) undoH.shift();
		redoH.resize(0);
	}

	public function clearHistory():Void {
		undoH.resize(0);
		redoH.resize(0);
	}

	function history(h1:Array<ArrHistory>, h2:Array<ArrHistory>):Void {
		final hid = h1.length - 1;
		if (hid == -1) return;
		final h = h1[hid];

		final olds = copyRect(h.rect, h.layer);
		fillTiles(h.rect, h.layer, h.tiles);

		h2.push({
			layer: h.layer,
			rect: h.rect,
			tiles: olds
		});
		h1.pop();
	}

	public function undo():Void {
		history(undoH, redoH);
	}

	public function redo():Void {
		history(redoH, undoH);
	}

	public function onMouseDown(p:Pointer, layer:Int, x:Int, y:Int, tile:Int):Void {
		start = {
			x: x,
			y: y
		};
		end = start;
	}

	public function onMouseMove(p:Pointer, layer:Int, x:Int, y:Int, tile:Int):Void {
		if (!p.isDown) return;
		end = {
			x: x,
			y: y
		};
	}

	public function onMouseUp(p:Pointer, layer:Int, x:Int, y:Int, tile:Int):Void {
		if (start == null) {
			end = null;
			return;
		}
		end = {
			x: x,
			y: y
		};
		fill(layer, tile);
		start = end = null;
	}

	public function onUpdate():Void {}

	function makeRect(p:IPoint, p2:IPoint):IRect {
		final sx = p.x < p2.x ? p.x : p2.x;
		final sy = p.y < p2.y ? p.y : p2.y;
		final ex = p.x < p2.x ? p2.x : p.x;
		final ey = p.y < p2.y ? p2.y : p.y;
		return {x: sx, y: sy, w: ex - sx, h: ey - sy};
	}

	public function onRender(g:Graphics):Void {
		if (start == null || end == null) return;
		g.color = 0xFFFF00FF;
		final rect = makeRect(start, end);
		final tileSize = tilemap.tileSize;
		g.drawRect(
			rect.x * tileSize + tilemap.camera.x - 1.5,
			rect.y * tileSize + tilemap.camera.y - 1.5,
			(rect.w + 1) * tileSize + 3, (rect.h + 1) * tileSize + 3
		);
	}

	function fill(layer:Int, tile:Int):Void {
		if (start == null || end == null) return;
		final rect = makeRect(start, end);

		final newObj = tilemap.objectTemplate(layer, tile);
		if (newObj != null) return;

		final olds = copyRect(rect, layer);
		fillRect(rect, layer, tile);

		addHistory({layer: layer, rect: rect, tiles: olds});
	}

	function copyRect(rect:IRect, layer:Int):Array<Array<Int>> {
		final arr:Array<Array<Int>> = [];
		for (iy in rect.y...rect.y + rect.h + 1) {
			final ty = iy - rect.y;
			arr[ty] = [];
			for (ix in rect.x...rect.x + rect.w + 1) {
				final tx = ix - rect.x;
				arr[ty][tx] = tilemap.getTile(layer, ix, iy).id;
			}
		}
		return arr;
	}

	function fillRect(rect:IRect, layer:Int, tile:Int):Void {
		for (iy in rect.y...rect.y + rect.h + 1)
			for (ix in rect.x...rect.x + rect.w + 1) {
				tilemap.setTileId(layer, ix, iy, tile);
			}
	}

	function fillTiles(rect:IRect, layer:Int, tiles:Array<Array<Int>>):Void {
		for (iy in rect.y...rect.y + rect.h + 1) {
			final ty = iy - rect.y;
			for (ix in rect.x...rect.x + rect.w + 1) {
				final tx = ix - rect.x;
				tilemap.setTileId(layer, ix, iy, tiles[ty][tx]);
			}
		}
	}

}
