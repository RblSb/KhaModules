package khm.editor;

import kha.Canvas;
import kha.graphics2.Graphics;
import kha.input.KeyCode;
import kha.Assets;
import kha.math.FastMatrix3;
import khm.editor.Interfaces.Tool;
import khm.tilemap.Tilemap;
import khm.tilemap.Tilemap.GameMap;
import khm.tilemap.Tilemap.GameMapJSON;
import khm.tilemap.Tile;
import khm.tilemap.Tileset;
import khm.imgui.Imgui;
import khm.utils.ScreenTools;
import khm.Screen;
import khm.Screen.Pointer;
import khm.utils.FileReference;
import khm.Lang;
import khm.Types.IPoint;
import khm.Types.Point;
import khm.Types.ISize;
import khm.Types.IRect;
import haxe.Json;
using khm.imgui.Widgets;

class Editor extends Screen {

	static inline var BTN_SIZE = 48;
	var ui = new Imgui({autoNotifyInput: false});
	var tilemap:Tilemap;
	var tileSize(get, never):Int;
	inline function get_tileSize():Int return tilemap.tileSize;
	@:nullSafety(Off) var tilePanel:TilePanel;
	@:nullSafety(Off) var arrow:Arrow;
	@:nullSafety(Off) var brush:Brush;
	@:nullSafety(Off) var fillRect:FillRect;
	@:nullSafety(Off) var pipette:Pipette;
	@:nullSafety(Off) var hand:Hand;
	var toolName = "";
	@:nullSafety(Off) var tool(default, set):Tool;
	function set_tool(tool:Tool):Tool {
		this.tool = tool;
		final cl = @:nullSafety(Off) Type.getClass(tool);
		final arr = Type.getClassName(cl).split(".");
		toolName = arr[arr.length - 1];
		return tool;
	}
	public var tilesLengths(get, never):Array<Int>;
	inline function get_tilesLengths():Array<Int> {
		return @:privateAccess tilemap.tileset.tilesLengths;
	}
	public var layer = 0;
	public var tile(get, set):Int;
	inline function get_tile():Int return tiles[layer];
	inline function set_tile(tile):Int {
		tiles[layer] = tile;
		return tile;
	}
	final tiles:Array<Int> = [];
	final cursor:IPoint = {x: 0, y: 0};
	final eraserMode = {tile: 0, layer: 0};
	var selection:Null<IRect>;
	var isGridEnabled = false;
	var x = 0;
	var y = 0;

	public function new(tileset:Tileset):Void {
		tilemap = new Tilemap(tileset);
		super();
	}

	public function init():Void {
		FileReference.removeOnDrop(onFileLoad);
		FileReference.onDrop(onFileLoad, false);

		final map = newMap({w: 10, h: 10});
		tilemap.loadMap(map);

		tilePanel = new TilePanel(this, tilemap);
		arrow = new Arrow(this, tilemap);
		brush = new Brush(this, tilemap);
		fillRect = new FillRect(this, tilemap);
		pipette = new Pipette(this, tilemap);
		hand = new Hand(this, tilemap);
		tool = brush;

		for (i in tilesLengths) tiles.push(0);
		onResize();
	}

	@:allow(khm.editor.Hand)
	function moveCamera(speed:Point):Void {
		tilemap.camera.x += speed.x;
		tilemap.camera.y += speed.y;
		updateCamera();
	}

	override function onKeyDown(key:KeyCode):Void {
		if (keys[Control] || keys[Meta]) {
			if (key == Z) {
				if (!keys[Shift]) tool.undo();
				else tool.redo();
			}
			if (key == Y) tool.redo();

			if (key == S) {
				keys[S] = false;
				keys[Control] = keys[Meta] = false;
				save(tilemap.map);
			}
		}
		if (ScreenTools.onRescaleKeys(this, key)) return;

		if (key == Left || key == Right || key == Up || key == Down) {
			if (!keys[Shift]) {
				moveCursor(key);
				updateCamera();
			}
		}

		if (key == Space) tilemap.setTileId(layer, x, y, tile); // TODO history
		else if (key == M) tool = arrow;
		else if (key == B) tool = brush;
		else if (key == R) tool = fillRect;
		else if (key == P) tool = pipette;
		else if (key == H) tool = hand;
		else if (key == O) browse();
		else if (key == N) createMap();
		else if (key == Q) prevTile();
		else if (key == E) nextTile();
		else if (key == G) isGridEnabled = !isGridEnabled;
		else if (key == Comma) tilePanel.incColumns();
		else if (key == Period) tilePanel.decColumns();
		else if (key == Zero) _testMap(this, tilemap);
		else if (key == Nine) resizeMap();
		else if (key == Backspace) clearSelection();

		else if (key - One >= 0 && key - One <= 9) {
			final newLayer = key - One;
			if (newLayer < tilemap.map.layers.length) layer = newLayer;

		} else if (key == Escape) {
			#if kha_html5
			final confirm = js.Browser.window.confirm;
			if (!confirm(Lang.get("dataWillBeLost") + " " + Lang.get("areYouSure"))) return;
			#end
			_exit();
		}
	}

	function moveCursor(key:KeyCode):Void {
		if (key == Left) x--;
		else if (key == Right) x++;
		else if (key == Up) y--;
		else if (key == Down) y++;
		cursorBounds();
		tilemap.camera.strictCenter({
			x: x * tileSize, y: y * tileSize,
			w: tileSize, h: tileSize
		});
	}

	static inline function _testMap(editor:Editor, tilemap:Tilemap):Void {
		if (testMap != null) testMap(editor, tilemap);
	}

	public static var testMap:Null<(editor:Editor, tilemap:Tilemap)->Void>;

	static inline function _exit():Void {
		if (exit != null) exit();
	}

	public static var exit:Null<()->Void>;

	override function onKeyUp(key:KeyCode):Void {}

	function prevTile():Void {
		tile--;
		if (tile < 0) tile = tilesLengths[layer];
		if (tile > tilesLengths[layer]) tile = 0;
	}

	function nextTile():Void {
		tile++;
		if (tile < 0) tile = tilesLengths[layer];
		if (tile > tilesLengths[layer]) tile = 0;
	}

	function createMap():Void {
		#if kha_html5
		final prompt = js.Browser.window.prompt;
		final newSize = Json.stringify({w: 20, h: 20});
		final size:ISize = Json.parse(prompt("Map Size:", newSize));
		if (size == null) return;
		final map = newMap(size);
		onMapLoad(map);
		#end
	}

	function newMap(size:ISize):GameMap {
		final layersLength = @:privateAccess tilemap.tileset.layersLength;
		final map:GameMap = {
			w: size.w,
			h: size.h,
			layers: [
				for (l in 0...layersLength) [
					for (iy in 0...size.h) [
						for (ix in 0...size.w) new Tile(tilemap, l, 0)
					]
				]
			],
			objects: [],
			floatObjects: []
		}
		return map;
	}

	function resizeMap():Void {
		#if kha_html5
		final prompt = js.Browser.window.prompt;
		final addSize = Json.stringify([0, 1, 0, 1]);
		final size:Array<Int> = Json.parse(
			prompt("Add Size [SX, EX, SY, EY]:", addSize)
		);
		if (size == null) return;
		final map = tilemap.map;
		for (l in 0...map.layers.length)
			resizeLayer(l, size, true);

		final sx = size[0];
		final ex = size[1];
		final sy = size[2];
		final ey = size[3];
		final newW = map.w + sx + ex;
		final newH = map.h + sy + ey;

		var i = 0;
		while (i < map.objects.length) {
			final obj = map.objects[i];
			obj.x += sx;
			obj.y += sy;
			if (obj.x < 0 || obj.y < 0 ||
				obj.x >= newW || obj.y >= newH) {
				map.objects.remove(obj);
				continue;
			}
			i++;
		}

		map.w = newW;
		map.h = newH;

		onMapLoad(map);
		#end
	}

	function resizeLayer(l:Int, size:Array<Int>, isFill:Bool):Void {
		final layer = tilemap.map.layers[l];
		final sx = size[0];
		final ex = size[1];
		final sy = size[2];
		final ey = size[3];
		inline function newTile():Tile {
			return new Tile(tilemap, l, 0);
		}

		final len = Std.int(Math.abs(sy));
		for (i in 0...len) {
			if (sy < 0) layer.shift();
			else {
				layer.unshift([]);
				for (tile in layer[1]) {
					final newTile = isFill ? tile.copy() : newTile();
					layer[0].push(newTile);
				}
			}
		}

		final len = Std.int(Math.abs(ey));
		for (i in 0...len) {
			if (ey < 0) layer.pop();
			else {
				layer.push([]);
				final h = layer.length - 1;
				for (tile in layer[h - 1]) {
					final tile = isFill ? tile.copy() : newTile();
					layer[h].push(tile);
				}
			}
		}

		final len = Std.int(Math.abs(sx));
		for (i in 0...len)
			for (iy in 0...layer.length) {
				if (sx < 0) layer[iy].shift();
				else {
					final tile = isFill ? layer[iy][0].copy() : newTile();
					layer[iy].unshift(tile);
				}
			}

		final len = Std.int(Math.abs(ex));
		for (i in 0...len) {
			for (iy in 0...layer.length) {
				if (ex < 0) layer[iy].pop();
				else {
					final w = layer[iy].length - 1;
					final id = isFill ? layer[iy][w].copy() : newTile();
					layer[iy].push(id);
				}
			}
		}
	}

	function save(map:GameMap, name = "map"):Void {
		final data = tilemap.toJSON(map);
		#if kha_html5
		FileReference.saveJson(name, Json.stringify(data));
		#else
		// TODO select path and write file
		#end
	}

	function browse():Void {
		#if kha_html5
		FileReference.browse(onFileLoad, false);
		#else
		// TODO browse path
		#end
	}

	public function onFileLoad(file:Any, name:String):Void {
		final ext = name.split(".").pop();
		final name = name.split(".")[0];
		switch (ext) {
			case "json":
				onMapJSONLoad(Json.parse(file), name);
			// case "lvl":
			// 	final bytes = haxe.io.Bytes.ofData(file);
			// 	final blob = kha.Blob.fromBytes(bytes);
			// 	final map = Old.loadMap(blob);
			// 	onMapLoad(map);
			default:
				trace('unknown file extension $ext');
		}
	}

	public function onMapJSONLoad(map:GameMapJSON, ?name:String):Void {
		onMapLoad(tilemap.fromJSON(map), name);
	}

	public function onMapLoad(map:GameMap, ?name:String):Void {
		if (name != null) map.name = name;
		tilemap.loadMap(map);
		clearHistory();
	}

	function clearHistory():Void {
		arrow.clearHistory();
		brush.clearHistory();
		fillRect.clearHistory();
		pipette.clearHistory();
		hand.clearHistory();
	}

	function clearSelection():Void {
		if (selection == null) { // TODO history in Arrow
			tilemap.setTileId(layer, x, y, 0);
			return;
		}
		final rect = selection;
		for (iy in rect.y...rect.y + rect.h + 1)
			for (ix in rect.x...rect.x + rect.w + 1)
				tilemap.setTileId(layer, ix, iy, 0);
	}

	function updateCursor(pointer:Pointer):Void {
		cursor.x = pointer.x;
		cursor.y = pointer.y;
		x = Std.int(cursor.x / tileSize - tilemap.camera.x / tileSize);
		y = Std.int(cursor.y / tileSize - tilemap.camera.y / tileSize);
		cursorBounds();
	}

	function cursorBounds():Void {
		if (x < 0) x = 0;
		if (y < 0) y = 0;
		if (x > tilemap.w - 1) x = tilemap.w - 1;
		if (y > tilemap.h - 1) y = tilemap.h - 1;
	}

	override function onMouseDown(p:Pointer):Void {
		if (tilePanel.onDown(p)) return;
		if (ui.onPointerDown(p)) return;
		updateCursor(p);
		if (keys[Alt] || p.type == 2) {
			tile = tilemap.getTile(layer, x, y).id;
			return;
		}
		if (p.type == 1) enableEraserMode();
		tool.onMouseDown(p, layer, x, y, tile);
	}

	override function onMouseMove(p:Pointer):Void {
		if (tilePanel.onMove(p)) return;
		if (ui.onPointerMove(p)) return;
		updateCursor(p);
		tool.onMouseMove(p, layer, x, y, tile);
	}

	override function onMouseUp(p:Pointer):Void {
		if (tilePanel.onUp(p)) return;
		if (ui.onPointerUp(p)) return;
		tool.onMouseUp(p, layer, x, y, tile);
		if (p.type == 1) disableEraserMode();
	}

	override function onMouseWheel(delta:Int):Void {
		if (delta == 1) prevTile();
		else if (delta == -1) nextTile();
	}

	override function onResize():Void {
		tilemap.camera.w = Screen.w;
		tilemap.camera.h = Screen.h;
		tilePanel.resize();
	}

	override function onUpdate():Void {
		tilePanel.update();
		tool.onUpdate();

		var sx = 0.0;
		var sy = 0.0;
		final s = tileSize / 5;
		if (keys[A]) sx += s;
		if (keys[D]) sx -= s;
		if (keys[W]) sy += s;
		if (keys[S]) sy -= s;

		if (keys[Shift]) {
			sx *= 2;
			sy *= 2;
			if (keys[Left]) moveCursor(Left);
			if (keys[Right]) moveCursor(Right);
			if (keys[Up]) moveCursor(Up);
			if (keys[Down]) moveCursor(Down);
		}
		if (sx != 0) tilemap.camera.x += sx;
		if (sy != 0) tilemap.camera.y += sy;
		if (keys[Space]) tilemap.setTileId(layer, x, y, tile);
		updateCamera();
	}

	function updateCamera():Void {
		final w = Screen.w;
		final h = Screen.h;
		final pw = tilemap.w * tileSize;
		final ph = tilemap.h * tileSize;
		final camera = tilemap.camera;
		final offset = BTN_SIZE;
		final maxW = w - pw - offset - tilePanel.w * tileSize;
		final maxH = h - ph - offset;

		if (camera.x > offset) camera.x = offset;
		if (camera.x < maxW) camera.x = maxW;
		if (camera.y > offset) camera.y = offset;
		if (camera.y < maxH) camera.y = maxH;
		if (pw < w - offset * 2 - tilePanel.w * tileSize) camera.x = w / 2 - pw / 2;
		if (ph < h - offset * 2) camera.y = h / 2 - ph / 2;
		camera.x = Std.int(camera.x);
		camera.y = Std.int(camera.y);
	}

	public function enableEraserMode():Void {
		eraserMode.layer = layer;
		eraserMode.tile = tile;
		tile = 0;
	}

	public function disableEraserMode():Void {
		layer = eraserMode.layer;
		tile = eraserMode.tile;
	}

	public inline function setSelection(rect:IRect):Void {
		selection = rect;
	}

	final tempMatrix = FastMatrix3.identity();

	override function onRender(frame:Canvas):Void {
		final g = frame.g2;
		g.begin(true, 0xFFBDC3CD);
		g.color = 0x50000000;
		g.drawRect(
			tilemap.camera.x - 0.5, tilemap.camera.y - 0.5,
			tilemap.w * tileSize + 1, tilemap.h * tileSize + 1
		);
		tilemap.drawLayers(g);

		drawSelection(g);
		tool.onRender(g);
		drawGrid(g);
		drawCursor(g);
		drawGUI(g);
		tilePanel.render(g);

		g.color = 0xFF000000;
		g.font = Assets.fonts.OpenSans_Regular;
		g.fontSize = 24;
		final temp = g.transformation;
		g.transformation = tempMatrix;
		final s = 'Layer: ${layer + 1} | Tile: $tile | Objects: ${tilemap.map.objects.length}';
		g.drawString(s, 0, 0);
		final fh = g.font.height(g.fontSize);
		g.drawString('$toolName | $x, $y', 0, fh);
		g.transformation = temp;

		g.end();
	}

	function drawGrid(g:Graphics):Void {
		if (!isGridEnabled) return;
		g.color = 0x15000000;
		for (ix in 0...tilemap.w) {
			for (iy in 0...tilemap.h) {
				g.drawRect(
					tilemap.camera.x + ix * tileSize + 0.5,
					tilemap.camera.y + iy * tileSize + 0.5,
					tileSize - 1, tileSize - 1
				);
			}
		}
	}

	function drawSelection(g:Graphics):Void {
		if (selection == null) return;
		final rect = selection;
		g.color = 0x88FFFF00;
		final tileSize = tilemap.tileSize;
		g.drawRect(
			rect.x * tileSize + tilemap.camera.x - 1.5,
			rect.y * tileSize + tilemap.camera.y - 1.5,
			(rect.w + 1) * tileSize + 3, (rect.h + 1) * tileSize + 3
		);
	}

	function drawCursor(g:Graphics):Void {
		if (tool == hand) return;
		g.color = 0x88000000;
		final px = x * tileSize + tilemap.camera.x;
		final py = y * tileSize + tilemap.camera.y;
		g.drawRect(px - 0.5, py - 0.5, tileSize + 1, tileSize + 1);
		if (tool == arrow) return;

		if (tile == 0) {
			g.color = 0x88FF0000;
			g.drawLine(px, py, px + tileSize, py + tileSize);
			g.drawLine(px + tileSize, py, px, py + tileSize);
		}
		g.color = 0x88FFFFFF;
		drawTile(
			g, layer,
			x * tileSize + tilemap.camera.x,
			y * tileSize + tilemap.camera.y,
			tile
		);
	}

	function drawGUI(g:Graphics):Void {
		ui.begin(g);
		// ui.debug = true;
		final i = Assets.images;
		final h = Std.int(BTN_SIZE / scale);
		ui.setButtonSize(h, h);
		ui.setColors(
			0x00FFFFFF, 0x44FFFFFF, 0x88FFFFFF,
			0x802082EA, 0xFFFFFFFF
		);
		if (ui.imageButton(0, h, i.editor_arrow)) onKeyDown(M);
		if (ui.imageButton(0, h * 2, i.editor_paint_brush)) onKeyDown(B);
		if (ui.imageButton(0, h * 3, i.editor_assembly_area)) onKeyDown(R);
		if (ui.imageButton(0, h * 4, i.editor_pipette)) onKeyDown(P);
		if (testMap != null) {
			if (ui.imageButton(Screen.w - h - tilePanel.w * tileSize, 0, i.editor_play)) onKeyDown(Zero);
		}

		if (Screen.isTouch) {
			if (ui.imageButton(0, h * 5, i.editor_hand)) onKeyDown(H);
			if (ui.imageButton(0, Screen.h - h, i.editor_undo)) {
				keys[Control] = true;
				onKeyDown(Z);
			}
			if (ui.imageButton(h, Screen.h - h, i.editor_redo)) {
				keys[Control] = true;
				onKeyDown(Y);
			}
		}
		ui.end();
	}

	public function drawTile(g:Graphics, layer:Int, x:Float, y:Float, id:Int):Void {
		if (id < 1) return;
		final tileset = @:privateAccess tilemap.tileset;
		id += tileset.layersOffsets[layer];
		final tx = (id % tileset.w) * tileSize;
		final ty = Std.int(id / tileset.w) * tileSize;
		g.drawSubImage(
			tileset.img, x, y,
			tx, ty, tileSize, tileSize
		);
	}

}
