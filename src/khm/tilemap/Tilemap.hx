package khm.tilemap;

import kha.graphics2.Graphics;
import khm.Types.Rect;

typedef GameMapJSON = {
	?version:Int,
	?name:String,
	w:Int,
	h:Int,
	layers:Array<Array<Array<Int>>>,
	objects:Array<GameObject>,
	floatObjects:Array<FloatObject>
}

typedef GameMap = { // map format
	?version:Int,
	?name:String,
	w:Int,
	h:Int,
	layers:Array<Array<Array<Tile>>>,
	objects:Array<GameObject>,
	floatObjects:Array<FloatObject>
}

typedef GameObject = {
	// grid-based
	type:String,
	layer:Int,
	x:Int,
	y:Int,
	data:Null<Dynamic>
}

typedef FloatObject = {
	type:String,
	rect:Rect,
	data:Null<Dynamic>
}

class Tilemap {

	static inline var DEF_SCALE = 1.0;
	@:nullSafety(Off) public var map(default, null):GameMap;
	public var w(get, never):Int;
	public var h(get, never):Int;
	inline function get_w():Int return map.w;
	inline function get_h():Int return map.h;
	public var tileSize(get, never):Int;
	inline function get_tileSize():Int return tileset.tileSize;
	public var camera = new Camera(DEF_SCALE);
	public var scale(default, set) = DEF_SCALE;
	function set_scale(scale:Float):Float {
		camera.scale = scale;
		return this.scale = scale;
	}
	var tileset:Tileset;
	final emptyTiles:Array<Tile> = [];

	public function new(tileset:Tileset) {
		inline setTileset(tileset);
	}

	public function setTileset(tileset:Tileset):Void {
		this.tileset = tileset;
		emptyTiles.resize(0);
		for (i in 0...tileset.layersLength) {
			emptyTiles.push(new Tile(this, i, 0));
		}
	}

	public function loadMap(map:GameMap):Void {
		this.map = copyMap(map);
	}

	public function loadJSON(map:GameMapJSON):Void {
		this.map = fromJSON(map);
		@:nullSafety(Off)
		final objects:Array<GameObject> = [
			for (obj in map.objects) copyGameObject(obj)
		];
		@:nullSafety(Off)
		final floatObjects:Array<FloatObject> = [
			for (obj in map.floatObjects) copyFloatObject(obj)
		];
		this.map.objects = objects;
		this.map.floatObjects = floatObjects;
	}

	function copyGameObject(obj:GameObject):GameObject {
		final data = obj.data == null ? null : Reflect.copy(obj.data);
		return {
			type: obj.type,
			layer: obj.layer,
			x: obj.x,
			y: obj.y,
			data: data
		}
	}

	function copyFloatObject(obj:FloatObject):FloatObject {
		final data = obj.data == null ? null : Reflect.copy(obj.data);
		return {
			type: obj.type,
			rect: obj.rect,
			data: data
		}
	}

	public function fromJSON(map:GameMapJSON):GameMap {
		final layers:Array<Array<Array<Tile>>> = [
			for (l in 0...map.layers.length) [
				for (iy in 0...map.layers[l].length) [
					for (ix in 0...map.layers[l][iy].length)
						new Tile(this, l, map.layers[l][iy][ix])
				]
			]
		];
		return {
			name: map.name,
			w: map.w,
			h: map.h,
			layers: layers,
			objects: map.objects,
			floatObjects: map.floatObjects
		}
	}

	public function toJSON(map:GameMap):GameMapJSON {
		final layers:Array<Array<Array<Int>>> = [
			for (l in 0...map.layers.length) [
				for (iy in 0...map.layers[l].length) [
					for (ix in 0...map.layers[l][iy].length)
						map.layers[l][iy][ix].id
				]
			]
		];
		return {
			name: map.name,
			w: map.w,
			h: map.h,
			layers: layers,
			objects: map.objects,
			floatObjects: map.floatObjects
		}
	}

	public function copyMap(map:GameMap):GameMap {
		final layers:Array<Array<Array<Tile>>> = [
			for (l in map.layers) [
				for (iy in 0...l.length) [
					for (ix in 0...l[iy].length)
						l[iy][ix].copy()
				]
			]
		];
		@:nullSafety(Off)
		final objects:Array<GameObject> = [
			for (obj in map.objects) copyGameObject(obj)
		];
		@:nullSafety(Off)
		final floatObjects:Array<FloatObject> = [
			for (obj in map.floatObjects) copyFloatObject(obj)
		];
		return {
			name: map.name,
			w: map.w,
			h: map.h,
			layers: layers,
			objects: objects,
			floatObjects: floatObjects
		}
	}

	public inline function isInside(x:Int, y:Int):Bool {
		return x > -1 && y > -1 && x < map.w && y < map.h;
	}

	public function getTile(layer:Int, x:Int, y:Int):Tile {
		if (!isInside(x, y)) return emptyTiles[layer];
		return map.layers[layer][y][x];
	}

	public function setTile(layer:Int, x:Int, y:Int, tile:Tile):Void {
		if (!isInside(x, y)) return;
		map.layers[layer][y][x] = tile;
	}

	public function setTileId(layer:Int, x:Int, y:Int, id:Int):Void {
		if (!isInside(x, y)) return;
		map.layers[layer][y][x].id = id;
	}

	public function drawLayer(g:Graphics, l:Int):Void {
		// screen in tiles
		final screenW = Math.ceil(camera.w / tileSize) + 1;
		final screenH = Math.ceil(camera.h / tileSize) + 1;
		// camera in tiles
		final ctx = -Std.int(camera.x / tileSize);
		final cty = -Std.int(camera.y / tileSize);
		final ctw = ctx + screenW;
		final cth = cty + screenH;
		//final camX = Std.int(camera.x * scale) / scale;
		//final camY = Std.int(camera.y * scale) / scale;
		final camX = camera.x;
		final camY = camera.y;

		// tiles offset
		final sx = ctx < 0 ? 0 : ctx;
		final sy = cty < 0 ? 0 : cty;
		final ex = ctw > map.w ? map.w : ctw;
		final ey = cth > map.h ? map.h : cth;
		g.color = 0xFFFFFFFF;

		for (iy in sy...ey) {
			for (ix in sx...ex) {
				final tile = map.layers[l][iy][ix];
				var id = tile.id;
				if (id > 0) {
					final layer = tile.layer;
					if (tile.frame > 0) {
						id = tileset.layersOffsets[layer];
						id += tileset.tilesLengths[layer];
						id += tileset.sprites[layer][tile.id].firstFrame + tile.frame;
					} else {
						id += tileset.layersOffsets[layer];
					}
					g.drawSubImage(
						tileset.img,
						ix * tileSize + camX,
						iy * tileSize + camY,
						(id % tileset.w) * tileSize,
						Std.int(id / tileset.w) * tileSize,
						tileSize, tileSize
					);
				}
			}
		}
	}

	public function drawLayers(g:Graphics):Void {
		for (i in 0...map.layers.length) drawLayer(g, i);
	}

	public function getObject(layer:Int, x:Int, y:Int, type:String):Null<GameObject> {
		for (obj in map.objects) {
			if (
				obj.x == x && obj.y == y &&
				obj.layer == layer && obj.type == type
			) return obj;
		}
		return null;
	}

	public function getObjects(layer:Int, x:Int, y:Int):Array<GameObject> {
		final arr:Array<GameObject> = [];
		for (obj in map.objects) {
			if (obj.x == x && obj.y == y && obj.layer == layer) arr.push(obj);
		}
		return arr;
	}

	public function setObject(layer:Int, x:Int, y:Int, type:String, data:Any):Void {
		if (data == null) {
			deleteObject(layer, x, y, type);
			return;
		}
		final obj = getObject(layer, x, y, type);
		if (obj != null) {
			obj.data = data;
			return;
		}
		map.objects.push({
			type: type,
			layer: layer,
			x: x,
			y: y,
			data: data
		});
	}

	public function setObjects(layer:Int, x:Int, y:Int, objs:Array<Null<GameObject>>):Void {
		final oldObjs = getObjects(layer, x, y);
		for (obj in oldObjs) map.objects.remove(obj);
		for (obj in objs) {
			if (obj != null) {
				if (obj.layer != layer) obj.layer = layer;
				if (obj.x != x) obj.x = x;
				if (obj.y != y) obj.y = y;
				map.objects.push(obj);
			}
		}
	}

	public function deleteObject(layer:Int, x:Int, y:Int, type:String):Void {
		final obj = getObject(layer, x, y, type);
		if (obj != null) map.objects.remove(obj);
	}

	public function objectTemplate(layer:Int, tile:Int):Null<GameObject> {
		return tileset.custom.objectTemplate(layer, tile);
	}

}
