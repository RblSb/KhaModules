package khm.tilemap;

import kha.graphics2.Graphics;
import kha.math.FastMatrix3;
import kha.FastFloat;
import kha.Blob;
import kha.Assets;
import kha.Image;

/** Tileset in JSON **/
private typedef TsTiles = {
	tileSize:Int,
	layers:Array<Array<TsProps>>
}

private typedef TsPropsMain = {
	?tx:Int, // grid-locked cords
	?ty:Int,
	?transformation:TsTransformation,
}

private typedef PreTsProps =
	TsPropsMain & {
	?props:Any,
	?id:Int,
	?file:String,
	?frames:Array<PreTsProps>,
	?x:Int,
	?y:Int
}

typedef TsProps =
	TsPropsMain & {
	props:Any, // tile type properties
	id:Int,
	file:String,
	frames:Array<TsProps>,
	x:Int,
	y:Int
}

enum abstract TsTransformation(String) {
	var Rotate90 = "rotate90";
	var Rotate180 = "rotate180";
	var Rotate270 = "rotate270";
	var FlipX = "flipX";
	var FlipY = "flipY";

	public static inline function getAngle(t:TsTransformation):Int {
		return switch (t) {
			case Rotate90: 90;
			case Rotate180: 180;
			case Rotate270: 270;
			default: throw 'wrong transformation: $t';
		}
	}
}

/** Tileset parsed data. **/
private typedef TsData = {
	tileSize:Int,
	layersOffsets:Array<Int>,
	tilesLengths:Array<Int>,
	layersLength:Int,
	sprites:Array<Array<TsSprite>>,
	props:Array<Array<Any>>,
	img:Image,
	w:Int,
	h:Int
}

private typedef TsSprite = {
	firstFrame:Int,
	length:Int,
	id:Int
}

class Tileset {

	public var props:Array<Array<Any>>;
	public var layersOffsets:Array<Int>;
	public var tilesLengths:Array<Int>;
	public var layersLength:Int;
	public var sprites:Array<Array<TsSprite>>;
	public var img(default, null):Image;
	public var w(default, null):Int;
	public var h(default, null):Int;
	public var tileSize(default, null):Int;
	public var custom:TsCustomizer;

	public function new(data:Blob, ?customizer:TsCustomizer):Void {
		custom = customizer == null ? new TsCustomizer() : customizer;
		final text = data.toString();
		final json:TsTiles = haxe.Json.parse(text);

		final generator = new TilesetGenerator();
		final ts:TsData = generator.fromJSON(json, custom);
		tileSize = ts.tileSize;
		layersOffsets = ts.layersOffsets;
		tilesLengths = ts.tilesLengths;
		layersLength = ts.layersLength;
		sprites = ts.sprites;
		props = ts.props;
		img = ts.img;
		w = ts.w;
		h = ts.h;
	}

	public function setCustomTexture(img:Image):Void {
		this.img = img;
	}

}

private class TilesetGenerator {

	static inline var MAX_SIZE = 1024 * 2;
	var custom:Null<TsCustomizer>;
	var props:Array<Array<Any>> = [];
	var w = -1;
	var h = -1;
	var tileSize = -1;

	var offset = 0;
	var x = 0;
	var y = 0;
	final next = {
		file: "",
		x: 0,
		y: 0
	};

	public function new():Void {}

	public function fromJSON(json:TsTiles, custom:TsCustomizer):TsData {
		this.custom = custom;
		tileSize = json.tileSize;
		// init file/x/y PreTsProps and cast
		final layers:Array<Array<TsProps>> = [
			for (layer in json.layers) [
				for (id in 0...layer.length) {
					fillProps(layer[id], id);
				}
			]
		];

		final layersLength = layers.length;
		final layersOffsets = [0]; // offsets in layers range
		final tilesLengths:Array<Int> = [ // tiles length in layers
			for (layer in layers) layer.length - 1
		];
		final sprites:Array<Array<TsSprite>> = [
			for (layer in layers) []
		];
		props = [ // props for every tile/frame
			for (layer in layers) []
		];

		final tilesCount = countTiles(layers);
		// w = Math.ceil(Math.sqrt(tilesCount));
		// h = Math.ceil(tilesCount / w);
		w = Std.int(MAX_SIZE / tileSize);
		h = Math.ceil(tilesCount / w);
		final img = Image.createRenderTarget(w * tileSize, h * tileSize);
		final g = img.g2;
		g.begin(true, 0x0);
		pushOffset();

		for (l in 0...layersLength) {
			final layer = layers[l];
			// empty tile properties for every layer
			final empty = layer.shift();
			if (empty == null) throw {
				'first tile is null (layer $l)';
			}
			addProps(l, empty);

			for (tile in layer) {
				addTile(g, l, tile);
			}

			// draw sprite frames after all layer tiles
			var spritesN = 0;
			var spriteOffset = 0;

			for (tile in layer) {
				final len = tile.frames.length;
				if (len == 0) continue;

				sprites[l][tile.id] = {
					firstFrame: spriteOffset,
					length: len,
					id: tile.id
				};
				spriteOffset += len;

				for (frame in tile.frames) {
					addTile(g, l, frame);
					spritesN++;
				}
			}

			// save layer offset
			final prev = layersOffsets[layersOffsets.length - 1];
			layersOffsets.push(prev + layer.length + spritesN);
		}
		g.end();
		return {
			tileSize: tileSize,
			layersOffsets: layersOffsets,
			tilesLengths: tilesLengths,
			layersLength: layersLength,
			sprites: sprites,
			props: props,
			img: img,
			w: w,
			h: h
		}
	}

	function fillProps(tile:PreTsProps, id:Int):TsProps {
		if (tile.props == null) tile.props = {};
		if (tile.id == null) tile.id = id;
		if (tile.frames == null) tile.frames = [];
		if (id == 0) return cast tile;
		initFilePath(tile);
		initTileCords(tile);
		for (frame in tile.frames) {
			if (frame.props == null) frame.props = {};
			initFilePath(frame);
			initTileCords(frame);
		}
		return cast tile;
	}

	function initFilePath(tile:PreTsProps):Void {
		if (tile.file != null) {
			tile.file = ~/(-|\/)/g.replace(tile.file, "_");
			if (next.file != tile.file) {
				next.x = 0;
				next.y = 0;
			}
			next.file = tile.file;
		} else {
			tile.file = next.file;
			if (Assets.images.get(tile.file) == null) trace(tile);
		}
	}

	function initTileCords(tile:PreTsProps):Void {
		final img = Assets.images.get(next.file);
		if (tile.x == null && tile.y == null) {
			if (tile.tx != null && tile.ty != null) {
				tile.x = tile.tx * tileSize;
				tile.y = tile.ty * tileSize;
			} else {
				tile.x = next.x;
				tile.y = next.y;
			}
		}
		next.x = tile.x + tileSize;
		next.y = tile.y;
		if (next.x > img.width - tileSize) {
			next.x = 0;
			next.y += tileSize;
		}
	}

	function addTile(g:Graphics, l:Int, tile:TsProps):Void {
		setTransformation(g, tile.transformation);
		drawTile(g, tile);
		g.transformation.setFrom(FastMatrix3.identity());
		addProps(l, tile);
		pushOffset();
	}

	function setTransformation(g:Graphics, transform:Null<TsTransformation>):Void {
		if (transform == null) return;
		switch (transform) {
			case Rotate90, Rotate180, Rotate270:
				final angle = TsTransformation.getAngle(transform);
				setRotation(g, angle);
			case FlipX:
				setFlipX(g);
			case FlipY:
				setFlipY(g);
			default:
				throw('unknown transformation: $transform');
		}
	}

	function countTiles(layers:Array<Array<TsProps>>):Int {
		var count = 1;
		for (layer in layers) {
			for (tile in layer) {
				count += 1 + tile.frames.length;
			}
		}
		return count;
	}

	function drawTile(g:Graphics, tile:TsProps):Void {
		final img = Assets.images.get(tile.file);
		g.drawSubImage(img, x, y, tile.x, tile.y, tileSize, tileSize);
	}

	function setRotation(g:Graphics, angle:Int):Void {
		g.transformation.setFrom(g.transformation.multmat(
			rotation(angle * Math.PI / 180, x + tileSize / 2, y + tileSize / 2)
		));
	}

	inline function rotation(angle:FastFloat, centerX:FastFloat, centerY:FastFloat): FastMatrix3 {
		return FastMatrix3.translation(centerX, centerY)
			.multmat(FastMatrix3.rotation(angle))
			.multmat(FastMatrix3.translation(-centerX, -centerY));
	}

	function setFlipX(g:Graphics):Void {
		g.transformation.setFrom(g.transformation.multmat(
			new FastMatrix3(
				-1, 0, x * 2 + tileSize,
				0, 1, 0,
				0, 0, 1
			)
		));
	}

	function setFlipY(g:Graphics):Void {
		g.transformation.setFrom(g.transformation.multmat(
			new FastMatrix3(
				1, 0, 0,
				0, -1, y * 2 + tileSize,
				0, 0, 1
			)
		));
	}

	function pushOffset():Void {
		offset += tileSize;
		x = offset % (w * tileSize);
		y = Std.int(offset / (w * tileSize)) * tileSize;
	}

	function addProps(l:Int, tile:TsProps):Void {
		if (custom == null) throw "customizer not found";
		final tileProps = custom.initProps(tile);
		props[l].push(tileProps);
	}

}
