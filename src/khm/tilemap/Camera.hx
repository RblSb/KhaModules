package khm.tilemap;

import kha.System;
import khm.Types.Rect;

class Camera {

	public var scale:Float;
	/**
		Disables scale-precision on getting camera rect values.
		Bad for drawing, good for operations before drawing.
	**/
	public var smooth = false;
	@:isVar public var x(get, set) = 0.0;
	function get_x():Float {
		if (smooth) return x;
		return Std.int(x * scale) / scale;
	}
	function set_x(n:Float):Float {
		return x = n;
	}
	@:isVar public var y(get, set) = 0.0;
	function get_y():Float {
		if (smooth) return y;
		return Std.int(y * scale) / scale;
	}
	function set_y(n:Float):Float {
		return y = n;
	}
	@:isVar public var w(get, set) = 0.0;
	function get_w():Float {
		if (smooth) return w;
		return Std.int(w * scale) / scale;
	}
	function set_w(n:Float):Float {
		return w = n;
	}
	@:isVar public var h(get, set) = 0.0;
	function get_h():Float {
		if (smooth) return h;
		return Std.int(h * scale) / scale;
	}
	function set_h(n:Float):Float {
		return h = n;
	}
	var shakeMaxTime = 0;
	var shakeTime = 0;
	var shakeMaxPower = 0.0;
	var shakePower = 0.0;

	public function new(scale:Float) {
		this.scale = scale;
		w = Std.int(System.windowWidth() / scale);
		h = Std.int(System.windowHeight() / scale);
	}

	public function set(rect:Rect):Void {
		x = rect.x;
		y = rect.y;
		w = rect.w;
		h = rect.h;
		update();
	}

	public function center(tilemap:Tilemap, rect:Rect):Void {
		final centerX = w / 2 - rect.x - rect.w / 2;
		final centerY = h / 2 - rect.y - rect.h / 2;
		final pw = tilemap.map.w * tilemap.tileSize;
		final ph = tilemap.map.h * tilemap.tileSize;

		if (pw < w) x = w / 2 - pw / 2;
		else if (x != centerX) {
			x = centerX;
			if (x > 0) x = 0;
			if (x < w - pw) x = w - pw;
		}
		if (ph < h) y = h / 2 - ph / 2;
		else if (y != centerY) {
			y = centerY;
			if (y > 0) y = 0;
			if (y < h - ph) y = h - ph;
		}
		update();
	}

	public function strictCenter(rect:Rect):Void {
		x = w / 2 - rect.x - rect.w / 2;
		y = h / 2 - rect.y - rect.h / 2;
		update();
	}

	public function update():Void {
		if (shakeTime > 0) shaking();
	}

	public function shake(power:Float, time:Int):Void {
		shakeTime = shakeMaxTime = time;
		shakeMaxPower = power;
		shakePower = 0;
	}

	var latestShakeX = 0.0;
	var latestShakeY = 0.0;

	function shaking():Void {
		var ratio = shakeTime / shakeMaxTime;
		if (ratio > 0.5) ratio = 1 - ratio;
		shakePower = shakeMaxPower * ratio;
		x -= latestShakeX;
		y -= latestShakeY;
		latestShakeX = Math.random() * shakePower * 2 - Math.random() * shakePower;
		latestShakeY = Math.random() * shakePower * 2 - Math.random() * shakePower;
		x += latestShakeX;
		y += latestShakeY;
		shakeTime--;
	}

}
