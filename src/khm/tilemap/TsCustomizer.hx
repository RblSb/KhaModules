package khm.tilemap;

import khm.tilemap.Tileset.TsProps;
import khm.tilemap.Tilemap.GameObject;

/** Called from Tileset for more flexible initialization **/
class TsCustomizer {

	public function new():Void {}

	/** Init every tile object from json. **/
	public function initProps(data:TsProps):Any {
		// final props = data.props;
		// if (props.collide == null) props.collide = false;
		// if (props.type == null) props.type = props.collide ? "FULL" : "NONE";
		return data.props;
	}

	/**
		Return default object for a specific layer and tile.
	**/
	public function objectTemplate(layer:Int, tile:Int):Null<GameObject> {
		return switch (layer) {
			case 0:
				switch (tile) {
					// case 0: {type: "chest", layer: layer, x: -1, y: -1, data: data}
					default: null;
				}
			default: null;
		}
	}

}
