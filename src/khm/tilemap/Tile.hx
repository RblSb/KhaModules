package khm.tilemap;

class Tile {

	public var layer(get, set):Int;
	public var id(get, set):Int;
	public var frame(default, null):Int;
	public var props(default, null):Any;
	public var frameCount(get, never):Int;
	var tilemap:Tilemap;
	var _layer:Int;
	var _id:Int;

	public function new(tilemap:Tilemap, layer:Int, id:Int, ?props:Any) {
		this.tilemap = tilemap;
		inline set(layer, id, props);
	}

	inline function get_layer():Int return _layer;

	inline function set_layer(layer:Int):Int {
		_layer = layer;
		frame = 0;
		setTileProps(layer, id);
		return _layer;
	}

	inline function get_id():Int return _id;

	inline function set_id(id:Int):Int {
		_id = id;
		frame = 0;
		setTileProps(layer, id);
		return _id;
	}

	inline function get_frameCount():Int {
		final tileset = @:privateAccess tilemap.tileset;
		return tileset.sprites[layer][id].length;
	}

	public function set(layer:Int, id:Int, ?props:Any):Void {
		_layer = layer;
		_id = id;
		frame = 0;
		if (props != null) setProps(props);
		else setTileProps(layer, id);
	}

	public inline function setProps(props:Any):Void {
		this.props = props;
	}

	public inline function setTileProps(layer:Int, id:Int):Void {
		props = @:privateAccess tilemap.tileset.props[layer][id];
	}

	public function setFrame(frame:Int):Void {
		this.frame = frame;

		final tileset = @:privateAccess tilemap.tileset;
		var frameId = tileset.tilesLengths[layer];
		frameId += tileset.sprites[layer][id].firstFrame + frame;
		props = tileset.props[layer][frameId];
	}

	public function setPrevFrame():Void {
		if (id > 1) setFrame(id - 1);
		else setFrame(frameCount);
	}

	public function setNextFrame():Void {
		if (id < frameCount) setFrame(id + 1);
		else setFrame(1);
	}

	public function copy():Tile {
		if (props == null) return new Tile(tilemap, layer, id);
		final newProps = Reflect.copy(props);
		return new Tile(tilemap, layer, id, newProps);
	}

}
