package khm;

import kha.System;
import kha.Assets;
import haxe.Json;

private typedef LangMap = Map<String, String>;

class Lang {

	public static var ids(default, null):Array<String> = [];
	public static var langs(default, null):Array<LangMap> = [];
	public static var iso(default, null) = "en";
	public static var fontGlyphs(default, null):Array<Int> = [];
	static var current(default, null):LangMap = [];
	static var basic(default, null):LangMap = [];

	public static function loadFolder(folder:String):Void {
		ids.resize(0);
		langs.resize(0);
		final fields = Reflect.fields(Assets.blobs);
		for (field in fields) {
			if (field.indexOf(folder) == -1) continue;
			final ereg = new EReg(folder + "_([A-z]+)_json$", "");
			if (!ereg.match(field)) continue;

			final id = ereg.matched(1);
			ids.push(id);
			final file = Assets.blobs.get(field);
			final data = Json.parse(file.toString());
			final lang = new LangMap();
			final keys = Reflect.fields(data);
			for (key in keys) {
				lang[key] = Reflect.field(data, key);
			}
			langs.push(lang);
		}
	}

	public static function set(?code:String, def = "en"):Void {
		iso = code == null ? System.language : code;
		var defId = -1;
		var id = -1;
		for (i in 0...ids.length) {
			final lang = ids[i];
			if (lang == def) defId = i;
			if (lang == iso) id = i;
		}

		if (defId == -1) throw 'default language file ($def) not found';
		if (id == -1) {
			iso = def;
			id = defId;
		}
		current = langs[id];
		basic = langs[defId];
		setGlyphs();
	}

	static function setGlyphs():Void {
		final code = iso.substr(0, 2);
		// recreating for new reference
		fontGlyphs = [];
		for (i in 32...127) {
			fontGlyphs.push(i);
		}
		switch (code) {
			case "ru": // without Ё
				for (i in 1040...1104) {
					fontGlyphs.push(i);
				}
				fontGlyphs.push(1105); // ё
			default:
		}
		fontGlyphs.push(8364); // €
	}

	public static function get(id:String):String {
		final s = current[id];
		if (s != null) return s;
		final s = basic[id];
		if (s != null) return s;
		return id;
	}

	public static inline function fastGet(id:String):Null<String> {
		return current[id];
	}

}
