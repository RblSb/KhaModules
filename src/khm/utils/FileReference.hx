package khm.utils;

import kha.System;
import kha.Assets;
import kha.Blob;
import haxe.io.Path;
#if kha_html5
import js.Browser.window;
import js.Browser.document;
import js.html.DragEvent;
import js.html.FileReader;
#end

private typedef FileLoadFunc = (data:Any, name:String)->Void;

class FileReference {

	static var onDropFunc:Null<FileLoadFunc>;
	static var isDropBinary = true;

	public static function onDrop(onFileLoad:FileLoadFunc, isBinary = true):Void {
		#if kha_html5
		function drop(e:DragEvent):Void {
			final file = e.dataTransfer.files[0];
			final reader = new FileReader();
			reader.onload = function(event) {
				onFileLoad(event.target.result, file.name);
			}
			e.preventDefault();
			if (isBinary) reader.readAsArrayBuffer(file);
			else reader.readAsText(file);
		}

		window.ondragenter = function(e) {
			e.preventDefault();
		};
		window.ondragover = function(e) {
			e.preventDefault();
		};
		window.ondrop = drop;
		#else
		onDropFunc = onFileLoad;
		isDropBinary = isBinary;
		System.notifyOnDropFiles(onDropFile);
		#end
	}

	static function onDropFile(path:String):Void {
		if (onDropFunc == null) return;
		Assets.loadBlobFromPath(path, function(blob:Blob) {
			final path = new Path(path);
			final name = path.file + path.ext;
			if (isDropBinary) onDropFunc(blob, name);
			else onDropFunc(blob.toString(), name);
		});
	}

	public static function removeOnDrop(onFileLoad:FileLoadFunc):Void {
		#if kha_html5
		window.ondrop = () -> false;
		#else
		onDropFunc = null;
		System.removeDropListerer(onDropFile);
		#end
	}

	public static function browse(onFileLoad:FileLoadFunc, isBinary = true):Void {
		#if kha_html5
		final input = document.createElement("input");
		input.style.visibility = "hidden";
		input.setAttribute("type", "file");
		input.id = "browse";
		input.onclick = function(e) {
			e.cancelBubble = true;
			e.stopPropagation();
		}
		input.onchange = function() {
			final file:Dynamic = (input:Dynamic).files[0];
			final reader = new FileReader();
			reader.onload = function(e) {
				onFileLoad(e.target.result, file.name);
				document.body.removeChild(input);

				/*final b = new js.html.Uint8ClampedArray(e.target.result);
				final blob = new js.html.Blob([b]);
				final url = js.html.URL.createObjectURL(blob);
				document.body.innerHTML = '<img src="'+url+'"/>';*/
			}
			if (isBinary) reader.readAsArrayBuffer(file);
			else reader.readAsText(file);
		}
		document.body.appendChild(input);
		input.click();
		#else
		#end
	}

	public static function saveJson(name:String, json:String):Void {
		#if kha_html5
		final blob = new js.html.Blob([json], {
			type: "application/json"
		});
		final url = js.html.URL.createObjectURL(blob);
		final a = document.createElement("a");
		untyped a.download = name + ".json";
		untyped a.href = url;
		a.onclick = function(e) {
			e.cancelBubble = true;
			e.stopPropagation();
		}
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
		js.html.URL.revokeObjectURL(url);
		#else
		// TODO select path and write file
		#end
	}

	public static function saveCanvas(name:String, w:Int, h:Int, pixels:haxe.io.Bytes):Void {
		#if kha_html5
		final canvas = document.createCanvasElement();
		canvas.width = w;
		canvas.height = h;
		final g = canvas.getContext("2d");
		g.imageSmoothingEnabled = false;
		final imgData = new js.html.ImageData(
			new js.lib.Uint8ClampedArray(pixels.getData()), w, h
		);
		g.putImageData(imgData, 0, 0);
		final url = canvas.toDataURL("image/png");
		// document.body.innerHTML = '<img src="'+url+'"/>';
		final a = document.createElement("a");
		untyped a.download = name + ".png";
		untyped a.href = url;
		a.onclick = function(e) {
			e.cancelBubble = true;
			e.stopPropagation();
		}
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
		js.html.URL.revokeObjectURL(url);
		#end
	}

	#if format
	public static function savePng(name:String, w:Int, h:Int, pixels:haxe.io.Bytes):Void {
		final out = new haxe.io.BytesOutput();
		final writer = new format.png.Writer(out);
		final argb = format.png.Tools.build32ARGB(w, h, pixels);
		writer.write(argb);
		final data = out.getBytes().getData();
	}

	#elseif upng
	public static function savePng(name:String, w:Int, h:Int, pixels:haxe.io.Bytes):Void {
		#if kha_html5
		final buffer = new js.html.Uint8ClampedArray(pixels.getData());
		final data = UPNG.encodeLL([buffer.buffer], w, h, 3, 1, 8);

		final blob = new js.html.Blob([data], {
			type: "image/png"
		});
		final url = js.html.URL.createObjectURL(blob);
		final a = document.createElement("a");
		untyped a.download = name + ".png";
		untyped a.href = url;
		a.onclick = function(e) {
			e.cancelBubble = true;
			e.stopPropagation();
		}
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
		js.html.URL.revokeObjectURL(url);
		#end
	}
	#end

}
