package khm.utils;

@:generic
class Pool<T> {

	var pool:Array<T>;
	var allocFunc:()->T;

	public function new(allocFunc:()->T) {
		this.allocFunc = allocFunc;
		pool = [];
	}

	public inline function push(t:T):Void {
		pool.push(t);
	}

	public inline function pop():T {
		if (pool.length == 0) return allocFunc();
		return pool.pop();
	}

}
