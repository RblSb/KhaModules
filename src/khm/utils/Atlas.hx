package khm.utils;

import kha.Image;
import kha.graphics2.Graphics;
import kha.Assets;
import kha.System;

private typedef FilterFunc = String->Bool;

@:structInit
class AtlasProps {

	public var scale = 1.0;
	public var widths:Null<Array<Int>> = null;

}

class Atlas {

	static final maxSize = 1024 * 2;
	@:nullSafety(Off) public static var atlas:Image;
	static final keys:Array<String> = [];
	static var props:Map<String, AtlasProps> = [];
	static var map:Map<Image, Int> = [];
	static final offsX:Array<Int> = [];
	static final offsY:Array<Int> = [];
	static final sizesW:Array<Int> = [];
	static final sizesH:Array<Int> = [];
	static final defaultProps:AtlasProps = {};

	public static function init(?imgFilter:FilterFunc, x = 0, y = 0):Void {
		initImagesMap(imgFilter);
		keys.sort(function(a, b) {
			final img = Assets.images.get(a);
			final img2 = Assets.images.get(b);
			final size = img.height;
			final size2 = img2.height;
			if (size > size2) return -1;
			else if (size < size2) return 1;
			return 0;
		});
		map = [
			for (i in 0...keys.length) Assets.images.get(keys[i]) => i
		];

		offsX.resize(0);
		offsY.resize(0);
		sizesW.resize(0);
		sizesH.resize(0);
		if (atlas == null) {
			atlas = Image.createRenderTarget(maxSize, maxSize);
		}

		final g = atlas.g2;
		var blockW = 0;
		var blockY = 0;
		var lineY = y;
		var lineH = 0;
		g.begin(true, 0x0);
		g.imageScaleQuality = High;

		for (i in 0...keys.length) {
			final name = keys[i];
			final img = Assets.images.get(name);
			final props = getProps(name);
			final imgW = getImageW(img, props);
			final imgH = getImageH(img, props);

			if (props.widths == null) {
				g.drawScaledImage(img, x, y, imgW, imgH);
			} else {
				props.widths = props.widths.copy();
				var subX = 0;
				var offX = x;
				for (i in 0...props.widths.length) {
					final w = props.widths[i];
					final scaledW = Math.ceil(w * props.scale);
					g.drawScaledSubImage(img,
						subX, 0, w, img.height,
						offX, y, scaledW, imgH
					);
					props.widths[i] = offX;
					offX += scaledW + 1;
					subX += w;
				}
			}

			offsX.push(x);
			offsY.push(y);
			sizesW.push(imgW);
			sizesH.push(imgH);
			if (i == keys.length - 1) break;

			final name = keys[i + 1];
			final next = Assets.images.get(name);
			final props = getProps(name);
			final nextW = getImageW(next, props);
			final nextH = getImageH(next, props);

			if (lineH < imgH) lineH = imgH;
			if (blockW < imgW) blockW = imgW;
			blockY += imgH + 1;

			if (blockY + nextH < lineH) { // down
				y = lineY + blockY;
				if (blockW < nextW) blockW = nextW;
			} else { // right and up
				x += blockW + 1;
				y = lineY;
				blockW = nextW;
				blockY = 0;
			}
			// new line
			if (x + nextW >= maxSize) {
				lineY += lineH + 1;
				x = 0;
				y = lineY;
				blockW = nextW;
				blockY = 0;
				lineH = 0;
			}
		}
		g.end();
	}

	static function getImageW(img:Image, props:AtlasProps):Int {
		if (props.widths == null) {
			return Math.ceil(img.width * props.scale);
		}
		var width = props.widths.length;
		for (w in props.widths) {
			width += Math.ceil(w * props.scale) + 1;
		}
		return width;
	}

	static inline function getImageH(img:Image, props:AtlasProps):Int {
		return Math.ceil(img.height * props.scale);
	}

	static inline function getProps(name:String):AtlasProps {
		final props = props[name];
		if (props == null) return defaultProps;
		return props;
	}

	public static inline function setProps(name:String, prop:AtlasProps):Void {
		props[name] = prop;
	}

	static function initImagesMap(?imgFilter:FilterFunc):Void {
		keys.resize(0);
		props = [];
		final fields = Reflect.fields(Assets.images);
		for (field in fields) {
			if (~/(Name|Description|names)$/.match(field)) continue;
			if (imgFilter == null) {
				keys.push(field);
				continue;
			}
			if (!imgFilter(field)) continue;
			keys.push(field);
		}
	}

	public static function drawAtlas(g:Graphics, x:Float, y:Float):Void {
		g.drawImage(atlas, x, y);
	}

	static function isInside(g:Graphics, x:Float, y:Float, w:Float, h:Float):Bool {
		final moveX = g.transformation._20;
		final moveY = g.transformation._21;
		final skewX = g.transformation._10;
		final skewY = g.transformation._01;
		if (moveX != 0 || moveY != 0) return true;
		if (skewX != 0 || skewY != 0) return true;
		final scaleX = g.transformation._00;
		final scaleY = g.transformation._11;

		return !(x + w < 0 || y + h < 0 ||
			x > System.windowWidth() / scaleX ||
			y > System.windowHeight() / scaleY
		);
	}

	static inline function mapGet(map:Map<Image, Int>, img:Image):Int {
		return @:nullSafety(Off) map[img];
	}

	public static function drawImage(g:Graphics, img:Image, x:Float, y:Float):Void {
		final id = mapGet(map, img);
		if (!isInside(g, x, y, sizesW[id], sizesH[id])) return;
		g.drawSubImage(atlas, x, y,
			offsX[id], offsY[id], sizesW[id], sizesH[id]
		);
	}

	public static function drawSubImage(
		g:Graphics, img:Image, x:Float, y:Float,
		sx:Float, sy:Float, sw:Float, sh:Float
	):Void {
		if (!isInside(g, x, y, sw, sh)) return;
		final id = mapGet(map, img);
		final scale = getProps(keys[id]).scale;
		g.drawSubImage(
			atlas, x, y,
			offsX[id] + sx * scale, offsY[id] + sy * scale,
			sw, sh
		);
	}

	public static function drawScaledImage(
		g:Graphics, img:Image, x:Float, y:Float, w:Float, h:Float
	):Void {
		if (!isInside(g, x, y, w, h)) return;
		final id = mapGet(map, img);
		g.drawScaledSubImage(
			atlas,
			offsX[id], offsY[id], sizesW[id], sizesH[id],
			x, y, w, h
		);
	}

	public static function drawScaledSubImage(
		g:Graphics, img:Image,
		sx:Float, sy:Float, sw:Float, sh:Float,
		x:Float, y:Float, w:Float, h:Float
	):Void {
		if (!isInside(g, x, y, w, h)) return;
		final id = mapGet(map, img);
		final scale = getProps(keys[id]).scale;
		g.drawScaledSubImage(
			atlas,
			sx * scale + offsX[id], sy * scale + offsY[id], sw * scale, sh * scale,
			x, y, w, h
		);
	}

	public static function drawScaledSubImagePart(
		g:Graphics, img:Image, part:Int,
		sx:Float, sy:Float, sw:Float, sh:Float,
		x:Float, y:Float, w:Float, h:Float
	):Void {
		if (!isInside(g, x, y, w, h)) return;
		final id = mapGet(map, img);
		final props = getProps(keys[id]);
		if (props.widths == null) throw {
			'props.widths is null on ${keys[id]}';
		}
		final partX = props.widths[part];
		final scale = props.scale;
		g.drawScaledSubImage(
			atlas,
			partX, sy * scale + offsY[id], sw * scale, sh * scale,
			x, y, w, h
		);
	}

}
