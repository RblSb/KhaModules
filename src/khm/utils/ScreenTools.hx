package khm.utils;

import kha.input.KeyCode;

class ScreenTools {

	public static function onRescaleKeys(screen:Screen, key:KeyCode):Bool {
		final scale = screen.scale;
		if (key == 189 || key == HyphenMinus) {
			if (scale > 1) screen.setScale(scale - 1);
			return true;
		} else if (key == 187 || key == Equals || key == Plus) {
			if (scale < 9) screen.setScale(scale + 1);
			return true;
		}
		return false;
	}

}
