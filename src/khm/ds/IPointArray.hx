package khm.ds;

import khm.Types.IPoint;

@:forward(resize)
abstract IPointArray(Array<Int>) {

	static inline var L = 2;

	public inline function new() {
		this = [];
	}

	public var length(get, never):Int;
	inline function get_length():Int {
		return Std.int(this.length / L);
	}

	public inline function pop():IPoint {
		final x = this.pop();
		final y = this.pop();
		return {x: x, y: y};
	}

	public inline function push(p:IPoint):Void {
		this.push(p.x);
		this.push(p.y);
	}

	@:arrayAccess
	public inline function get(i:Int):IPoint {
		return {x: this[i * L], y: this[i * L + 1]};
	}

}
